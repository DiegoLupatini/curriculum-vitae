/*First slideshow*/

var slideIndex1 = 1;
showSlides1(slideIndex1);

function plusSlides1(n) {
  showSlides1(slideIndex1 += n);
}

function currentSlide1(n) {
  showSlides1(slideIndex1 = n);
}

function showSlides1(n) {
  var i;
  var slides1 = document.getElementsByClassName("projectSlides1");
  var dots1 = document.getElementsByClassName("projectDot1");
  if (n > slides1.length) {slideIndex1 = 1}    
  if (n < 1) {slideIndex1 = slides1.length}
  for (i = 0; i < slides1.length; i++) {
      slides1[i].style.display = "none";  
  }
  for (i = 0; i < dots1.length; i++) {
      dots1[i].className = dots1[i].className.replace(" active", "");
  }
  slides1[slideIndex1-1].style.display = "block";  
  dots1[slideIndex1-1].className += " active";
}

/*Second slideshow*/


var slideIndex2 = 1;
showSlides2(slideIndex2);

function plusSlides2(m) {
  showSlides2(slideIndex2 += m);
}

function currentSlide2(m) {
  showSlides2(slideIndex2 = m);
}

function showSlides2(m) {
  var j;
  var slides2 = document.getElementsByClassName("projectSlides2");
  var dots2 = document.getElementsByClassName("projectDot2");
  if (m > slides2.length) {slideIndex2 = 1}   
  if (m < 1) {slideIndex2 = slides2.length}
  for (j = 0; j < slides2.length; j++) {
      slides2[j].style.display = "none";  
  }
  for (j = 0; j < dots2.length; j++) {
      dots2[j].className = dots2[j].className.replace(" active", "");
  }
  slides2[slideIndex2-1].style.display = "block";  
  dots2[slideIndex2-1].className += " active";
}

/*Onscroll function*/

window.onscroll = function(){

  var top = window.pageYOffset;
  var half = 0.5*this.screen.height;
  var offAbout = this.document.getElementById("about").offsetTop;    
  var offEducation = this.document.getElementById("education").offsetTop;    
  var offExperience = this.document.getElementById("experience").offsetTop;
  var offAchievements = this.document.getElementById("achievements").offsetTop;
  var offSkills = this.document.getElementById("skills").offsetTop;    
  var offProjects = this.document.getElementById("projects").offsetTop;      
  var offPassions = this.document.getElementById("passions").offsetTop;    
  var offContact = this.document.getElementById("contact").offsetTop;

  if(top > offAbout - half && top < offEducation - half + 150){
    this.document.getElementById("linkAbout").style.color = "var(--pal3)";
    this.document.getElementById("linkAbout").style.fontWeight = "bolder";
    this.document.getElementById("linkAbout").style.letterSpacing = "1px";
  }else{
    this.document.getElementById("linkAbout").style.color = "var(--pal2)";
    this.document.getElementById("linkAbout").style.fontWeight = "normal";
    this.document.getElementById("linkAbout").style.letterSpacing = "0px";
  } 
  
  if(top > offEducation - half + 150 && top < offExperience - half){
    this.document.getElementById("linkEducation").style.color = "var(--pal3)";
    this.document.getElementById("linkEducation").style.fontWeight = "bolder";
    this.document.getElementById("linkEducation").style.letterSpacing = "1px";
  }else{
    this.document.getElementById("linkEducation").style.color = "var(--pal2)";
    this.document.getElementById("linkEducation").style.fontWeight = "normal";
    this.document.getElementById("linkEducation").style.letterSpacing = "0px";
  }

  if(top > offExperience - half && top < offAchievements - half){
    this.document.getElementById("linkExperience").style.color = "var(--pal3)";
    this.document.getElementById("linkExperience").style.fontWeight = "bolder";
    this.document.getElementById("linkExperience").style.letterSpacing = "1px";
  }else{
    this.document.getElementById("linkExperience").style.color = "var(--pal2)";
    this.document.getElementById("linkExperience").style.fontWeight = "normal";
    this.document.getElementById("linkExperience").style.letterSpacing = "0px";
  }

  if(top > offAchievements - half && top < offSkills - half){
    this.document.getElementById("linkAchievements").style.color = "var(--pal3)";
    this.document.getElementById("linkAchievements").style.fontWeight = "bolder";
    this.document.getElementById("linkAchievements").style.letterSpacing = "1px";
  }else{
    this.document.getElementById("linkAchievements").style.color = "var(--pal2)";
    this.document.getElementById("linkAchievements").style.fontWeight = "normal";
    this.document.getElementById("linkAchievements").style.letterSpacing = "0px";
  }
  
  if(top > offSkills - half && top < offProjects - half){
    this.document.getElementById("linkSkills").style.color = "var(--pal3)";
    this.document.getElementById("linkSkills").style.fontWeight = "bolder";
    this.document.getElementById("linkSkills").style.letterSpacing = "1px";
  }else{
    this.document.getElementById("linkSkills").style.color = "var(--pal2)";
    this.document.getElementById("linkSkills").style.fontWeight = "normal";
    this.document.getElementById("linkSkills").style.letterSpacing = "0px";
  }
  
  if(top > offProjects - half && top < offPassions- half){
    this.document.getElementById("linkProjects").style.color = "var(--pal3)";
    this.document.getElementById("linkProjects").style.fontWeight = "bolder";
    this.document.getElementById("linkProjects").style.letterSpacing = "1px";
  }else{
    this.document.getElementById("linkProjects").style.color = "var(--pal2)";
    this.document.getElementById("linkProjects").style.fontWeight = "normal";
    this.document.getElementById("linkProjects").style.letterSpacing = "0px";
  }

  if(top > offPassions - half && top < offContact - half){
    this.document.getElementById("linkPassions").style.color = "var(--pal3)";
    this.document.getElementById("linkPassions").style.fontWeight = "bolder";
    this.document.getElementById("linkPassions").style.letterSpacing = "1px";
  }else{
    this.document.getElementById("linkPassions").style.color = "var(--pal2)";
    this.document.getElementById("linkPassions").style.fontWeight = "normal";
    this.document.getElementById("linkPassions").style.letterSpacing = "0px";
  }

  if(top > offContact - half){
    this.document.getElementById("linkContact").style.color = "var(--pal3)";
    this.document.getElementById("linkContact").style.fontWeight = "bolder";
    this.document.getElementById("linkContact").style.letterSpacing = "1px";
  }else{
    this.document.getElementById("linkContact").style.color = "var(--pal2)";
    this.document.getElementById("linkContact").style.fontWeight = "normal";
    this.document.getElementById("linkContact").style.letterSpacing = "0px";
  }

}

/*Moving navigation bar on mobile*/

const navSlide = () => {
  const burger = document.querySelector('.burger');
  const nav = document.querySelector('.links');

  burger.addEventListener('click', () => {
    //mostra e nascondi barra laterale
    nav.classList.toggle('nav-active');
    //crea la croce nel burger
    burger.classList.toggle('cross');
  });

  nav.addEventListener('click', () => {
    //nascondi barra laterale alla selezione
    nav.classList.toggle('nav-active');
    //togli croce alla selezione
    burger.classList.toggle('cross');
  });
}
navSlide();
 
/* Loading Screen*/

window.addEventListener("load", function () {
  const loader = document.querySelector(".loader");
  loader.className += " hidden";
});